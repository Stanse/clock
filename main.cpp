#include <algorithm>
#include <cstdlib> // для использования exit()
#include <fstream>
#include <iostream>
#include <string>
#include <ctime>
#include "engine.hpp"

extern GLfloat lastFrame;
extern GLfloat deltaTime;

using namespace std;

int main()
{
    int screen_w = 680;
    int screen_h = 680;
    Engine engine(screen_w, screen_h);
    engine.init();

      // ====================
  // Colored MARK
  // ====================
  Game_object mark;
  glm::vec3 mark_color = glm::vec3(12.0/255.0, 34.0/255.0, 56.0/255.0);
  mark.posx = 0;
  mark.posy = engine.get_screen_h();;
  mark.scale_xy = 100.0f; 

    Sprite time_sprite;
    time_t c_time = time(nullptr);
    tm *local_time = nullptr;
    string hour;
    string min;
    string sec;
    string time;
    char const* p_char_hour = nullptr;

    Sprite boy;
    int num_of_anim = 0;
    float animation_time  = 80;
    string num_of_anim_string;
    char const* p_anim = nullptr;

    engine.bind_sprites(time_sprite);
    engine.bind_game_object(mark);
    // ====================
    // GAME LOOP
    // ====================
    bool run                = true;
    engine.camera.camera_on = false;

    while (run)
    {
        engine.calc_delta_time();
        engine.read_input(run);
        engine.do_movement(run);
        engine.clear_display();
        engine.clear_display();


        c_time = std::time(nullptr);
        local_time = localtime(&c_time);
        hour = to_string(local_time->tm_hour);
        if(local_time->tm_min < 10)
            min = "0" + to_string(local_time->tm_min);
        else
            min = to_string(local_time->tm_min);

        if(local_time->tm_sec < 10)
            sec = "0" + to_string(local_time->tm_sec);
        else
            sec = to_string(local_time->tm_sec);

        time = hour + ":" + min + ":" + sec;
        p_char_hour = time.c_str();

        time_sprite.diffuseMap = engine.get_texture_from_text(p_char_hour);
        time_sprite.translate(screen_w/2.5f, 200, 0.0f);
        time_sprite.scale(100, 50, 1.0f);

        engine.draw_sprite(time_sprite);


        if(num_of_anim > 20)
            num_of_anim = 0;
        num_of_anim_string = "res/anim/" + std::to_string(num_of_anim) + ".gif";
        p_anim = num_of_anim_string.c_str();
        boy.diffuseMap = engine.load_sprite_img(boy, p_anim);
        boy.translate(400, screen_h/1.5, 0.0f);
        boy.scale(80, 150, 1.0f);

        engine.draw_sprite(boy);
        animation_time -= deltaTime;
        if(animation_time < 0)
        {
            animation_time = 80;
            num_of_anim++;
        }

     mark.translate(0, mark.posy, 0.0f);
     mark.scale(mark.scale_xy);
     engine.draw_game_obj_ortho(mark, mark_color);

        engine.swap_buffers();
    }


    engine.quit();

    return 0;
}


