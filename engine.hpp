#pragma once
#include <SDL.h>
#include <SDL2/SDL_ttf.h>
#include <vector>

#include "camera.hpp"
#include "game_object.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "shader.hpp"
#include <cstdlib> // for functions rand() and srand()
#include <ctime>   // for time()

class Sprite
{
public:
    Sprite();
    std::vector<GLfloat> vertices;
    unsigned char*       image = nullptr;
    GLuint               textureID;

    GLuint    vbo;
    GLuint    diffuseMap  = 0;
    GLuint    specularMap = 0;
    glm::mat4 model;
    glm::mat4 view;
    glm::mat4 projection;
    int       width, height, nrComponents;

    void translate(GLfloat x, GLfloat y, GLfloat z);
    void scale(GLfloat x, GLfloat y, GLfloat z);
    void scale(GLfloat scale);
};

class Engine
{
public:
    Engine(int, int);
    ~Engine();

    int          init();
    void         clear_display();
    void         swap_buffers();
    void         print_opengl_version();
    unsigned int load_sprite_img(Sprite& sprite, const char* path);
    unsigned int load_texture(Game_object& game_obj, const char* path);
    void         bind_sprites(Sprite& sprite);
    void         bind_game_object(Game_object& game_obj);
    void         bind_letters();
    void         draw_sprite(Sprite& sprite);
    void         draw_game_obj(Game_object& game_obj, glm::vec3& color);
    void         draw_game_obj_ortho(Game_object& game_obj, glm::vec3& color);
    void         draw_letter(Game_object& game_obj);
    unsigned int get_texture_from_text(const char* text);
    void         validate_opengl_errors();
    int          quit();

    void read_input(bool& run);
    void do_movement(bool& run);

    glm::vec3 get_random_color()
    {
        srand(static_cast<unsigned int>(time(0)));

        GLfloat r = (GLfloat)(rand()) / (GLfloat)RAND_MAX + 0.1f;
        GLfloat g = (GLfloat)(rand()) / (GLfloat)RAND_MAX + 0.1f;
        GLfloat b = (GLfloat)(rand()) / (GLfloat)RAND_MAX + 0.1f;
        return glm::vec3(r, g, b);
    }

    GLfloat calc_delta_time();

    float get_time() { return SDL_GetTicks(); }
    void  set_screen_w(int w) { screen_width = w; }
    void  set_screen_h(int h) { screen_height = h; }
    int   get_screen_w() { return screen_width; }
    int   get_screen_h() { return screen_height; }

    bool delay_us(GLfloat start_point, GLuint delay)
    {
        if (start_point + get_time() >= delay)
        {
            return true;
        }
        else
            return false;
    }

    Shader* shader_font         = nullptr;
    Shader* shader_gui          = nullptr;
    Shader* shader_lighting     = nullptr;
    Shader* shader_font_img     = nullptr;
    Shader* shader_simple_color = nullptr;

    std::vector<Game_object> game_objects;
    GLuint                   light_VBO, light_VAO;
    glm::vec3                lightPos;
    GLuint                   game_obj_VBO, game_obj_VAO;
    GLuint                   gui_VBO, gui_VAO;
    GLuint                   sprite_VBO, sprite_VAO;

    SDL_Surface* text_surface = nullptr;
    TTF_Font*    font         = nullptr;
    SDL_Color    font_color;
    GLuint       font_texture   = 0;
    GLuint       uniformSampler = 0;

    std::vector<GLfloat> gui_vertices;
    void                 add_vert_to_VBO_buffer(std::vector<GLfloat>& vertices)
    {
        gui_vertices.reserve(gui_vertices.size() + vertices.size());
        gui_vertices.insert(gui_vertices.end(), vertices.begin(),
                            vertices.end());
    }

    SDL_Event e;
    bool      keys[1024]  = { false };
    bool      first_mouse = true;
    GLfloat   lastX;
    GLfloat   lastY;
    Camera    camera;
    GLint     cursor_xpos, cursor_ypos;

    bool left_mouse_click = false;

private:
    int           screen_width, screen_height;
    SDL_Window*   window     = nullptr;
    SDL_GLContext gl_context = nullptr;
};
